package com.mindex.challenge.service.impl;

import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Employee;
import com.mindex.challenge.data.Report;
import com.mindex.challenge.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportServiceImpl implements ReportService {

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);
    int numberOfReports = 0;

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Report read(String id) {
        LOG.debug("Creating employee with id [{}]", id);

        Employee employee = employeeRepository.findByEmployeeId(id);
        Employee reports = null;
        if (employee == null) {
            throw new RuntimeException("Invalid employeeId: " + id);
        }
        //Left my debug comments in - Pete Lounsbury
        if(employee.getDirectReports() != null) {
            numberOfReports = employee.getDirectReports().size();

            System.out.println("NUMBER OF REPORTS: " + numberOfReports);
            for (int i = 0; i < employee.getDirectReports().size(); i++) {
                reports = employeeRepository.findByEmployeeId(employee.getDirectReports().get(i).getEmployeeId());
                System.out.println("employee ID " + employee.getDirectReports().get(i).getEmployeeId());
                if (reports.getDirectReports() != null) {
                    numberOfReports = numberOfReports + reports.getDirectReports().size();
                }
            }
        }
        else
        {
            numberOfReports = 0;
        }

        System.out.println("AFTER FOR LOOP: " + numberOfReports);
        Report report = new Report();
        report.setEmployeeId(id);
        report.setNumberOfReports(numberOfReports);
        System.out.println(employee.getDirectReports());
       return report;
    }
}

