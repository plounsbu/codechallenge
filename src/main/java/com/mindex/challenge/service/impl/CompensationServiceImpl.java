package com.mindex.challenge.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.mindex.challenge.dao.EmployeeRepository;
import com.mindex.challenge.data.Compensation;
import com.mindex.challenge.service.CompensationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

@Service
public class CompensationServiceImpl implements CompensationService {
    private static final Logger LOG = LoggerFactory.getLogger(CompensationServiceImpl.class);
    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public Compensation create(Compensation compensation) {
        Compensation newCompensation = new Compensation();
        LOG.debug("Creating compensation [{}]", "test");
        newCompensation.setEmployee(compensation.getEmployee());
        newCompensation.setSalary(compensation.getSalary());
        newCompensation.setEffectiveDate(Calendar.getInstance());
        System.out.println("create compensation");

        ObjectMapper mapper = new ObjectMapper();
        try {
            File f = new File("compensationDB.json");
            if (f.length() != 0) {
                final String newComp = mapper.writeValueAsString(newCompensation);
                JsonNode tree1 = mapper.readTree("[" + newComp + "]");
                JsonNode tree2 = mapper.readTree(f);
                System.out.println("TREE1: " + "[" + tree1.toString());
                System.out.println("TREE2: " + tree2.toString());

                ((ArrayNode) tree2).addAll((ArrayNode) tree1);

                String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tree2);
                Files.write(new File("compensationDB.json").toPath(), Arrays.asList(json), StandardOpenOption.CREATE);
            } else {
                final String newComp = mapper.writeValueAsString(newCompensation);
                Files.write(new File("compensationDB.json").toPath(), Arrays.asList("[" + newComp + "]"), StandardOpenOption.CREATE);
            }
        } catch (Exception e) {
            e.getMessage();
        }
        return newCompensation;
    }


    @Override
    public Compensation read(String id) {
        Compensation compensation = null;
        List<Compensation> compensationList = null;
        LOG.debug("Getting compensation with id [{}]", id);
        ObjectMapper mapper = new ObjectMapper();
        try {
            compensationList = Arrays.asList(mapper.readValue(new File("compensationDB.json"), Compensation[].class));

        } catch (Exception e) {
            e.getMessage();
        }

        if (compensationList != null) {

            for (int i = 0; i < compensationList.size(); i++) {
                if (compensationList.get(i).getEmployee().getEmployeeId().equalsIgnoreCase(id)) {
                    compensation = compensationList.get(i);
                }
            }
        } else {
            System.out.println("Compensation not found for ID: " + id);
        }
        return compensation;
    }
}
