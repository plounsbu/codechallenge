package com.mindex.challenge.service;

import com.mindex.challenge.data.Report;
public interface ReportService {
    Report read(String id);
}
