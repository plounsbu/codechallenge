package com.mindex.challenge.controller;

import com.mindex.challenge.data.Report;
import com.mindex.challenge.service.ReportService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ReportingStructureController {
    private static final Logger LOG = LoggerFactory.getLogger(ReportingStructureController.class);
    @Autowired
    private ReportService reportService;

    @GetMapping("/report/{id}")
    public Report read(@PathVariable String id) {
        LOG.debug("Received reporting create request for id [{}]", id);

        return reportService.read(id);
    }
}
